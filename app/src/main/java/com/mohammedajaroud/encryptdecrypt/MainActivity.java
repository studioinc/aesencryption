package com.mohammedajaroud.encryptdecrypt;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mohammedajaroud.encryptionlib.AESHelper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText input, output, outputDecrypt;
    AESHelper aesHelper;
    Button add, clean, encrypt, decrypt;
    String textEncrypt, textDecrypt, textOutputDecrypt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        aesHelper = new AESHelper();

        //Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
       // setSupportActionBar(toolbar);
       // toolbar.setBackgroundColor(Color.parseColor("#D6D7D7"));

        input = (EditText)findViewById(R.id.editText_input);
        output = (EditText)findViewById(R.id.editText_output);
        outputDecrypt = (EditText)findViewById(R.id.editText_output_decrypt);

        textEncrypt = input.getText().toString().trim();
        textDecrypt = output.getText().toString().trim();
        textOutputDecrypt = outputDecrypt.getText().toString().trim();

        add = (Button)findViewById(R.id.btn_add);
        clean = (Button)findViewById(R.id.btn_clean);
        encrypt = (Button)findViewById(R.id.btn_encrypt);
        decrypt = (Button)findViewById(R.id.btn_decrypt);

        add.setOnClickListener(this);
        clean.setOnClickListener(this);
        encrypt.setOnClickListener(this);
        decrypt.setOnClickListener(this);

        aesHelper.encrypt("Text to be encrypt");
        input.setText("Text to be encrypt");
        output.setText(aesHelper.encrypt("Text to be encrypt"));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_add:
                if (textEncrypt != null){
                    textEncrypt = input.getText().toString().trim();
                    aesHelper.encrypt(textEncrypt);
                    output.setText(aesHelper.encrypt(textEncrypt));
                    Toast.makeText(this, "Added Success!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "No Added", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btn_clean:
                input.setText("");
                output.setText("");
                break;
            case R.id.btn_encrypt:
                if (aesHelper.getEncrypt() == null) {
                    Toast.makeText(this, "No Found Text Encrypt", Toast.LENGTH_LONG).show();
                } else {
                    textEncrypt = input.getText().toString().trim();
                    output.setText(aesHelper.encrypt(textEncrypt));
                }
                break;
            case R.id.btn_decrypt:
                if (aesHelper.getEncrypt() == null){
                    Toast.makeText(this, "No Found Text Encrypt", Toast.LENGTH_LONG).show();
                } else {

                    textDecrypt = output.getText().toString().trim();
                    aesHelper.encrypt(textDecrypt);

                    outputDecrypt.setText(aesHelper.decrypt(aesHelper.isDecrypted()));
                    Toast.makeText(this, aesHelper.decrypt(aesHelper.isDecrypted()), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public void onClick1(View view) {
        input.setText("");
    }

    public void onClick2(View view) {
        output.setText("");
    }

    public void onClick3(View view) {
        outputDecrypt.setText("");
    }
}
