package com.mohammedajaroud.encryptionlib;

/**
 * Created by Develop on 6/16/2017.
 */
public final class AESHelper {
    public static final String CryptKey = "s>5$KVMcp46!h/vkHa{X@SYt@5!";
    public static final String CryptSecret = "A%KtQ_z{khRP6Are_{sY_s3<]r$JnA%,EW8V";
    public static String encrypted, decrypted, isdecrypt, key = CryptKey, secret = CryptSecret;
    public static byte[] sizeData = new byte[16];

    public static String encrypt(String encrypt){
        Encryption encryption = Encryption.getDefault(key, secret, sizeData);
        encrypted = encryption.encryptOrNull(encrypt);
        return encrypted;
    }

    public static String decrypt(String decrypt){
        Encryption encryption = Encryption.getDefault(key, secret, sizeData);
        decrypted = encryption.decryptOrNull(decrypt);
        return decrypted;
    }

    public static String isDecrypted(){
        Encryption encryption = Encryption.getDefault(key, secret, sizeData);
        isdecrypt = encryption.decryptOrNull(encrypted);
        return isdecrypt;
    }


    public static String getEncrypt(){
        return encrypted;
    }


}